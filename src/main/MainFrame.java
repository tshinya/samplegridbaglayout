package main;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import component.WestJPanel;

public class MainFrame extends JFrame {

	public MainFrame() {
		super("GridBagLayoutSample");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.add(new WestJPanel(), BorderLayout.WEST);
	}
}
