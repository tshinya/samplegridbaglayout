package component;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;

public class WestJPanel extends JPanel {
	
	public WestJPanel() {
		this.setPreferredSize(new Dimension(100, 600));
		this.setBackground(Color.WHITE);
		GridBagLayout layout = new GridBagLayout();
		this.setLayout(layout);
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 1.0;
		gbc.fill = GridBagConstraints.BOTH;
		
		TopJPanel topPanel = new TopJPanel();
		gbc.gridy = 0;
		gbc.weighty = 0.2;
		layout.setConstraints(topPanel, gbc);
		
		MiddleJPanel middlePanel = new MiddleJPanel();
		gbc.gridy = 1;
		layout.setConstraints(middlePanel, gbc);
		
		BottomJPanel bottomPanel = new BottomJPanel();
		gbc.gridy = 2;
		gbc.weighty = 0.6;
		layout.setConstraints(bottomPanel, gbc);
		
		this.add(topPanel);
		this.add(middlePanel);
		this.add(bottomPanel);
	}

}
